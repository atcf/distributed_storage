#!/usr/bin/env python
# -*- coding: utf-8 -*-



#=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
#title           :easy_eos.py
#author          :Alexis Schreiber
#email		 :aschreib@cern.ch
#date            :18th March 2014
#version         : 1.0
#=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-







#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@====Import-Section====@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

#----------------------------------
#	import os
#--Used to launch commands
import os


#-----------------------------------
#	import re
#--Allows to use Regular expressions
import re



#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@------------------------------------------------@@@@
#@@@@====This section contains Classes definition====@@@@
#@@@@------------------------------------------------@@@@
#@@@@@@@@@@@@@@@@@@@@@@\\@@@@//@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@\\@@//@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@\\//@@@@@@@@@@@@@@@@@@@@@@@@@@@@


# ==================================================
# ++This classe is used to color the text outputed++
# ==================================================

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'

    def disable(self):
        self.HEADER = ''
        self.OKBLUE = ''
        self.OKGREEN = ''
        self.WARNING = ''
        self.FAIL = ''
        self.ENDC = ''






# ======================================================
# This class allows to define each parameters of a disk
# e.g. :
#	- label="data01"
#	- sd="sda"
#	- sg="sg5"
#	- eos_found="true"
#	- config_status="rw"
#	- boot_status="booted"
#	- drain_status="nodrain"
#	- channel="16:0"
#	- serial ="WCAVY67393"
#	- slot="0"
#	- enclosure="0"
#	- device_id="null" (only if tere is a megaraid controller)
#
#	By default the values are:
#
#	- label="not_found"
#	- sd="not_found"
#	- sg="not_found"
#	- eos_found="false"
#	- config_status="not_found"
#	- boot_status="not_found"
#	- drain_status="not_found"
#	- channel="not found"
#	- serial ="not found"
#	- slot="not found"
#	- enclosure="not found"
## ======================================================





class Disk:

    def __init__(self,label="not_found",sd="unknown",sg="unknown",eos_found="false",config_status="not_found",boot_status="not_found",drain_status="not_found",channel="not_found",slot='not_found',serial='not_found',enclosure="not_found",device_id="null") :
        self.label=label
        self.sd=sd
        self.sg=sg
        self.eos_found=eos_found
        self.config_status=config_status
        self.drain_status=drain_status
        self.boot_status=boot_status
        self.channel=channel
        self.slot=slot
        self.serial=serial
        self.enclosure=enclosure
        self.device_id=device_id





# ======================================================
# This class allows to define each parameters size of a disk
# e.g. :
#	- label=12
#	- sd=4
#	- sg=4
#	- eos_found=9
#	- config_status=3
#	- boot_status=7
#	- drain_status=10
#	- channel=6
#	- serial =9
#	- slot=7
#	- enclosure=0
## ======================================================
class Size:

    def __init__(self,label=0,sd=0,sg=0,eos_found=0,config_status=0,boot_status=0,drain_status=0,channel=0,slot=0,serial=0,enclosure=1):

        self.label=label
        self.eos_found=eos_found
        self.config_status=config_status
        self.boot_status=boot_status
        self.drain_status=drain_status
        self.sd=sd
        self.sg=sg
        self.channel=channel
        self.slot=slot
        self.enclosure=enclosure
        self.serial=serial

        self.size_list=[label,eos_found,config_status,boot_status,drain_status,sd,sg,sg,channel,channel,slot,enclosure,serial]


#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@------------------------------------------------@@@@
#@@@@===This section contains Functions definition===@@@@
#@@@@------------------------------------------------@@@@
#@@@@@@@@@@@@@@@@@@@@@@\\@@@@//@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@\\@@//@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@\\//@@@@@@@@@@@@@@@@@@@@@@@@@@@@



# ======================================================
# This function is able to find the controller type
# By default the controller is unknown
# the commande "lspci" is launched
# if 'Adaptec' is find in the output controller="Adaptec"
# else if 'MegaRAID' is find in the output controller="MegaRAID"
# else if 'LSI' is find in the output controller="LSI"
# else if '3ware' is find in the output controller="3ware"
## ======================================================

def controller_detector ():
    controller_list=['Adaptec','MegaRAID','3ware']
    controller="unknown"

    lspci= os.popen('lspci').read()
    i=0
    for line in controller_list:

        if lspci.find(controller_list[i])!=-1:
            controller=controller_list[i]
            break

        i+=1

    return controller



# ================================================================
# This function launches  smarttools
# the first parameters is the smarttool the user would like to use
# The second parameters is the device
# e.g.
#	- smartctl="smartctl -a"
#	- device="sg12"
#
#
# the function check the controller
# 	If the controller  is adaptec,or a megaRAID it verify that
#	the device format is like "sg.."
#
#		If yes it builds the command with parameters and
#		stores it in the var "cmd"
#
#		The output of the commande is stored in the var output
#		it print the value of output
#
#		If not it reports an error
#
#	If the controller is not an adaptec it reports that the
#	curent version does not suport it
# ================================================================

def smarttool (controller,smartctl,device) :


    if controller == "Adaptec":

        if re.findall(r'\b(sg)+[0-9]{1,2}',device.sg):

                cmd='smartctl '+smartctl+  ' -d sat /dev/'+device.sg

            print '\n\t'+cmd+'\n'
            output=os.popen(cmd).read()
            print output

        else :
            print "\n"
            print bcolors.FAIL+"[ ERROR ] =>"+bcolors.ENDC+" logical device name fault "
            print "You have to check it by hand"

    elif controller =="MegaRAID":

        cmd='smartctl '+smartctl+  ' -d sat+megaraid,'+device.device_id+' /dev/'+device.sd
        print '\n\t'+cmd+'\n'
        output=os.popen(cmd).read()
        print output

    elif  controller == "3ware" or controller == "LSI":
        print "\n"
        print bcolors.FAIL+controller+" not yet supported"+bcolors.ENDC

    else :

        print "this controller is unknow, please report this issue"






#==================================================================
# This function is able to find fs whish sould be mount
# The output of this function is consider has the truth table
# In the first part it launches 'cat /etc/fstab|grep data'
# reads the output and split it
# each line of the output it retrives strings which contain the
# string data followed by two two digits
# It adds this information inside a list "disk_table"
# disk_table is return
#==================================================================
def fstab_retriever():
    disk_table=[]
    fstab= os.popen('cat /etc/fstab|grep data').read().split('\n')
    for line in fstab:

        regex=re.findall(r'\bdata[0-9]{2}',line)

        if regex :
            for info in regex:

                    label=info
            disk_table.append(Disk(label))

    return disk_table



# ================================================================
# Set the default value of output to "not_found"
# launches a command
# read the output of the command
# find the value
# ================================================================
def fill_object (cmd,value_grep,find_value,stop_value) :
    output="not_found"
    df=os.popen(cmd+'| grep '+value_grep ).read()

    if df.find(value_grep)!= -1 :
        output=""
        k=int(df.find(find_value))+5
        while df[k]!=stop_value :
            output=output+df[k]
            k+=1
    return output



# ================================================================
# This function will try to find the sg and sd link to each
# file system
# It take one argument:disk_table(a list of Disk object)
# for each label already store in disk_table[x].label
# launched the function fil_object()
#
# ================================================================


def sd_and_sg_retriver(disk_table) :
    i=0
    for lab in disk_table :

        disk_table[i].sd=fill_object("df ",disk_table[i].label,"/dev/","1")
        disk_table[i].sg=fill_object("lsscsi -g ",disk_table[i].sd,"/dev/sg","\n")
        i+=1

    return disk_table



# ================================================================
# This function will try to find the channel of disks
# It take one argument:disk_table(a list of Disk object)
# for each sg already store in disk_table[x].sg
# launched the lsscsi -g | grep disk_table[x].sg and store it cmd
# Step if disk_table[x].sg is present in cmd
#	it finds a string which is bound by [ ]
#	it delets [ and 1or 2 digit : on or two digit :
#	it delet the last ]
#	The value is store in disk_table[x].channel
# Disk_table is return
# ================================================================

def channel_retriver(disk_table):
    i=0
    for line in disk_table:
        cmd=os.popen('lsscsi -g| grep '+line.sg ).read()
        if cmd.find(line.sg)!=-1:
            find=re.findall(r'^\[.{0,}\]',cmd)
            find=re.sub(r'^\[\d{1,2}:\d{1,2}:','',find[0])
            find=re.sub(r'\]$','',find)
            disk_table[i].channel=find
        i+=1

    return disk_table



# ================================================================
# This function will try to find the channel of disks
# It take two argument:
#	- disk_table(a list of Disk object)
#	- controller (controler type)
# If the controller is Adaptec:
# 	It launches 'arcconf getconfig 1' read it and split it
# 	This list is stored in 'conf'
#	 for each element of disk_table
#		for each element of conf
#			if the channel containd in disk_table is find in conf
#				retrive the serial of the disk and his Slot
# If the controller is MegaRAID:
# 	It launches 'megacli -LdPdInfo -a0' read it and split it
# 	This list is stored in 'conf'
#	 for each element of disk_table
#		for each element of conf
#			if the channel containd in disk_table is find in conf
#				retrive the serial of the disk and his Slot
#=================================================================



def slot_serial_enclo_retriver(controller,disk_table):
    serial_disk=""
    slot_disk=""
    hwraidman_info=os.popen('hwraidman info').read().split("\n")




    if controller=='Adaptec':
        i=0
        arcconf=os.popen('arcconf getconfig 1').read()
        arcconf=re.sub(r'Device #','splitDevice #',arcconf)
        arcconf=arcconf.split('split')


#in case of raid

        for line in disk_table:

            for hwraidman_info_line in hwraidman_info:
                    if re.match('.{0,}RAID.{0,}',hwraidman_info_line)is not None and (re.findall(disk_table[i].sd,hwraidman_info_line)):
                    disk_table[i].channel="Raid"
                    disk_table[i].slot="Raid"
                    disk_table[i].serial="Raid"
                    disk_table[i].enclosure="Raid"
                    break

            for conf in arcconf:
                if conf.find("("+line.channel+")")!=-1:
                    if re.findall(r'Serial number\s{0,}:\s{0,}.{1,}\b',conf) :

                        serial=re.findall(r'Serial number\s{0,}:\s{0,}.{1,}\b',conf)[0]
                        serial=re.sub(r'Serial number\s{0,}:\s{0,}','',serial)
                        enclosure=re.findall(r'Enclosure \d{1}',conf)[0]
                        disk_table[i].enclosure=re.sub(r'Enclosure\s{0,}','',enclosure)
                        disk_table[i].serial=serial
                        disk_table[i].slot=re.findall(r'Slot \d{1,2}',conf)[0]
                        break

            i+=1



    elif controller=='MegaRAID':

        megacli=os.popen('megacli -LdPdInfo -a0').read().split("Virtual Drive")
        i=0

        for line in disk_table:


#in case of raid
            for hwraidman_info_line in hwraidman_info:
                    if re.match('.{0,}RAID-1',hwraidman_info_line)is not None and (re.findall(disk_table[i].sd,hwraidman_info_line)):
                    disk_table[i].channel="Raid"
                    disk_table[i].slot="Raid"
                    disk_table[i].serial="Raid"
                    disk_table[i].enclosure="Raid"
                    break

            target_id=re.sub(r':\d{1,2}','',line.channel)
            for conf in megacli:
                if conf.find("(Target Id: "+target_id+")")!=-1:
                    slot=re.findall(r'Slot Number\s{0,}:\s{0,}\d{1,}\b',conf)[0]
                    slot=re.sub(r'Slot Number\s{0,}:\s{0,}', '', slot)
                    disk_table[i].slot=slot

                    enclosure=re.findall(r'Enclosure Device ID:\s\d{1,2}',conf)[0]
                    enclosure=re.sub(r'Enclosure Device ID:\s','',enclosure)
                    disk_table[i].enclosure=enclosure

                    serial=re.findall(r'Inquiry Data\s{0,}:\s{0,}[A-Z0-9]{1,}',conf)[0]
                    serial=re.sub(r'[A-Z]$','',serial)
                    serial=re.sub(r'Inquiry Data\s{0,}:\s{0,}','',serial)
                    disk_table[i].serial=serial

                    device_id=re.findall(r'Device Id:\s\d{1,2}',conf)[0]
                    device_id=re.sub(r'Device Id:\s','',device_id)
                    disk_table[i].device_id=device_id

                    break



            i+=1
    return disk_table



#==================================================================
# This function is able to find the config status,boot status,
# the drain status of each eos file system but also if the filesystem
# present in /etc/fstab is also present in eosfsview (eos_found)
#It take one argument:disk_table(a list of Disk objects)
#
#In the first part there are 3 lists
#
# the first one contains config status that we are looking for
# the second one contains boot status that we are looking for
# the third one contains drains status that we are looking for
#
#
# In the first part of the function it is launching the command:
# "eosadmin -b node ls -l $HOSTNAME",read the output and split it by \n
#
# In the second part, it is checking if the disk label is present in the
# output of eosadmin -b node ls -l $HOSTNAME
#
# In the third part for each line of this output it is looking if :
#
# - the bootstatus is in bootstatus list
# - the config status is in configstatus list
# - the drain status is in drainstatus list
#if not, the value by default is the value sets in Disk object
#
#At the end the function returns the list disk_table which is now fills
# with the configstatus,the bootstatus,drainstatus and if the file system
#is present eosview and /etc/fstab
#
#==================================================================
def eos_info_retriver(disk_table):
    configstatus=[' rw ',' ro ',' off ',' drain ',' empty ']
    bootstatus=[' booted ',' booting ',' opserror ',' bootfailure ',' prepare ','down']
    drainstatus=[' nodrain ',' draining ',' drained ',' stalling ',' expired ']

    eos_info=os.popen("eosadmin -b node ls -l $HOSTNAME").read().split("\n")

    for line in eos_info:

        i=0

        for info in disk_table:

            if line.find(disk_table[i].label)!=-1:

                disk_table[i].eos_found="true"


                for stat in configstatus:

                    if line.find(stat)!=-1:

                        disk_table[i].config_status=stat
                        break

                for boot in bootstatus:

                    if line.find(boot)!=-1:

                        disk_table[i].boot_status=boot
                        break

                for drain in drainstatus:

                    if line.find(drain)!=-1:

                        disk_table[i].drain_status=drain
                        break

            i+=1

    return disk_table



def fill_disk_table () :

    # disk_table will containt Disk object
    disk_table=[]

    #----------------------------------------------
    # Calls of functions defined in function Section
    #----------------------------------------------


    # Init the var of the contoller
    controller=controller_detector()

    #==========================================================
    #It calls the function fstab_retriever
    #   which stores label inside disk_table.label
    disk_table=fstab_retriever()

    #==========================================================
    #It calls the function sd_and_sg_retriver
    #   which stores sd inside disk_table.sd
    #   which stores sg inside disk_table.sg
    disk_table=sd_and_sg_retriver(disk_table)

    #==========================================================
    #It calls the function channel_retriver
    #   which stores channel inside disk_table.channel
    disk_table=channel_retriver(disk_table)

    #==========================================================
    #It calls the function eos_info_retriver
    #	which store eos_found inside disk_table.eos_found
    #   which stores the boot status inside disk_table.boot_status
    #	which stores the config status inside disk_table.config_status
    #	which stores the drain status inside disk_table.drain_status
    disk_table=eos_info_retriver(disk_table)

    #==========================================================
    #It calls the function slot_serial_enclo_retriver
    #   which store disk serial number inside disk_table.serial
    #   which store disk slot number inside disk_table.slot
    disk_table=slot_serial_enclo_retriver(controller,disk_table)
    return disk_table




#==================================================================
# This function finds the max value
#==================================================================

def size_retriver(string1_size,string2_size):

    if string1_size < len(string2_size) :
        string1_size=len(string2_size)

    return string1_size


#==================================================================
# The goal of this function is to format all datas store inside
# disk table
# In the first part it formats the string which is on the top of the
# board and prints it
# In the second part, it formats the datas from disk_table and prints it
#==================================================================

def view_format(size,disk_table):


    menu=['label','is in eos','config status','boot status','drain status','device','logical','channel','Slot','Enclosure','Serial']
    output_menu="| "


    for i in range (len(menu)):

        if len(menu[i])<size.size_list[i]:
            menu[i]=menu[i]+" "*(size.size_list[i]-len(menu[i]))
        else:
            size.size_list[i]=len(menu[i])

    i=0
    for line in menu:

        output_menu=output_menu+line+" "*(size.size_list[i]-len(line))+" | "

    output_menu=output_menu+"ID |"

    print output_menu
    print "="*len(output_menu)



    for i in range(len(disk_table)):

        disk_table_output="| "

        disk_table_output=disk_table_output+disk_table[i].label+" "*(size.size_list[0]-len(disk_table[i].label))+" | "
        if disk_table[i].eos_found!= "true":
            disk_table_output=disk_table_output+bcolors.WARNING+disk_table[i].eos_found+bcolors.ENDC+" "*(size.size_list[1]-len(disk_table[i].eos_found))+" | "
        else:
            disk_table_output=disk_table_output+disk_table[i].eos_found+" "*(size.size_list[1]-len(disk_table[i].eos_found))+" | "
        if disk_table[i].config_status!=" rw ":
            disk_table_output=disk_table_output+bcolors.WARNING+disk_table[i].config_status+bcolors.ENDC+" "*(size.size_list[2]-len(disk_table[i].config_status))+" | "
        else:
            disk_table_output=disk_table_output+disk_table[i].config_status+" "*(size.size_list[2]-len(disk_table[i].config_status))+" | "

        if disk_table[i].boot_status!=" booted ":
            disk_table_output=disk_table_output+bcolors.WARNING+disk_table[i].boot_status+bcolors.ENDC+" "*(size.size_list[3]-len(disk_table[i].boot_status))+" | "
        else:
            disk_table_output=disk_table_output+disk_table[i].boot_status+" "*(size.size_list[3]-len(disk_table[i].boot_status))+" | "

        if disk_table[i].drain_status!= " nodrain ":
            disk_table_output=disk_table_output+bcolors.WARNING+disk_table[i].drain_status+bcolors.ENDC+" "*(size.size_list[4]-len(disk_table[i].drain_status))+" | "
        else:
            disk_table_output=disk_table_output+disk_table[i].drain_status+" "*(size.size_list[4]-len(disk_table[i].drain_status))+" | "



        disk_table_output=disk_table_output+disk_table[i].sd+" "*(size.size_list[5]-len(disk_table[i].sd))+" | "
        disk_table_output=disk_table_output+disk_table[i].sg+" "*(size.size_list[6]-len(disk_table[i].sg))+" | "
        disk_table_output=disk_table_output+disk_table[i].channel+" "*(size.size_list[7]-len(disk_table[i].channel))+" | "
        disk_table_output=disk_table_output+disk_table[i].slot+" "*(size.size_list[8]-len(disk_table[i].slot))+" | "
        disk_table_output=disk_table_output+disk_table[i].enclosure+" "*(size.size_list[9]-len(disk_table[i].enclosure))+" | "
        disk_table_output=disk_table_output+disk_table[i].serial+" "*(size.size_list[10]-len(disk_table[i].serial))+" | "+str(i)+" |"

        print disk_table_output
        print "."*len(output_menu)

    return 0
#==================================================================
# This function display the welcome message
#==================================================================
def welcome_message():

    message=bcolors.OKBLUE+"\tThis is beta version, in case of problem please report it to aschreib@cern.ch"+bcolors.ENDC
    print "="*len(message)
    print message
    print "="*len(message)+"\n"
    print "\n"




# ======================================================
# This function displays the main view
# ======================================================
#def main_view (controller,disk_table) :
def main_view () :

    controller=controller_detector()
    disk_table=fill_disk_table()


    print "\n"
    welcome_message()
    print "\n"
    print "\t\tThe controller is : "+bcolors.OKGREEN +controller+bcolors.ENDC
    print "\n"


        #------------------------------------------------------
        # It determines the size of each element of disk_table
        # Only the max value is stored inside the object size
        # which is of type Size()
        # This part is only to be have a nice output
        # Data inside disk_table are JUST read
        # it does not modify it
        #--------------------------------------------

    size=Size()


    i=0
    for x in disk_table:


        size.label=size_retriver(size.label,disk_table[i].label)
        size.eos_found=size_retriver(size.eos_found,disk_table[i].eos_found)
        size.config_status=size_retriver(size.config_status,disk_table[i].config_status)
        size.boot_status=size_retriver(size.boot_status,disk_table[i].boot_status)
        size.drain_status=size_retriver(size.drain_status,disk_table[i].drain_status)
        size.sd=size_retriver(size.sd,disk_table[i].sd)
        size.sg=size_retriver(size.sg,disk_table[i].sg)
        size.channel=size_retriver(size.channel,disk_table[i].channel)
        size.slot=size_retriver(size.slot,disk_table[i].slot)
        size.enclosure=size_retriver(size.enclosure,disk_table[i].enclosure)
        size.serial=size_retriver(size.serial,disk_table[i].serial)

        i+=1


        size.size_list=[size.label,size.eos_found,size.config_status,size.boot_status,size.drain_status,size.sd,size.sg,size.channel,size.slot,size.enclosure,size.serial]


    view_format(size,disk_table)

        #--------------------------------------------
        # This part displays the values contained in
        # a list of Disk() object
        #--------------------------------------------


    print "\n"



#==================================================================
# This function display the Menu
#==================================================================

def menu(controller,disk_table):

    choice_bool ="false"
    while choice_bool =="false" :

        print "\n"
        print "\t\t "+"-"*28
        print "\t\t|"+" "*12+bcolors.WARNING+"Menu"+bcolors.ENDC+" "*12+"|"
        print "\t\t|"+"-"*28+"|"
        print "\t\t| 1 : main view              |"
        print "\t\t| 2 : start long smart test  |"
        print "\t\t| 3 : start short smart test |"
        print "\t\t| 4 : display smart Test     |"
        print "\t\t| 5 : display eos info       |"
        print "\t\t| 0 : Exit                   |"
        print "\t\t "+"-"*28
        print "\n"
        choice=raw_input('Enter your choice : ')
        if choice == '1':
            main_view ()

        elif choice == '2' :
            id=raw_input('\n\tEnter the Disk id  : ')
            smarttool (controller,'-t long' ,disk_table[int(id)])

        elif choice == '3' :
            id=raw_input('\n\tEnter the Disk id  : ')
            smarttool (controller,'-t short' ,disk_table[int(id)])

        elif choice == '4' :
            id=raw_input('\n\tEnter the Disk id  : ')
            smarttool (controller,'-a' ,disk_table[int(id)])

        elif choice == '5' :
            output=os.popen('eosadmin -b node ls -l $HOSTNAME').read()

            print output
        elif choice == '0' :
            choice_bool ="true"
        else :
            print "choice not inside the list"

    return 0




#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@---------------------------------@@@@@@@@@@@
#@@@@@@@@===This is the start of the script===@@@@@@@@@
#@@@@@@@@@@----------------------------------@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@\\@@@@//@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@\\@@//@@@@@@@@@@@@@@@@@@@@@@@@@@
#@@@@@@@@@@@@@@@@@@@@@@@\\//@@@@@@@@@@@@@@@@@@@@@@@@@@@




#::::::::::::::::::::::::::::
#:::::::::INIT:::::::::::::::
#::::::::::::::::::::::::::::

controller=controller_detector()

disk_table=fill_disk_table()


#//Display the main view of the script
main_view ()

#display the menu
menu(controller,disk_table)
