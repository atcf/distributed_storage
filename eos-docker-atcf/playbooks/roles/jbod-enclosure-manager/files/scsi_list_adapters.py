#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import re
import argparse
import textwrap
from os.path import basename, dirname, isfile
from operator import attrgetter

class HostBusAdapter:
    def __init__(self, scsi_host_number="null", name="unknown", sas_wwid="not_found", firmware_version="not_found", sysfs_dir="not_found", sysfs_device_dir="not_found", pci_device_info="not_found", phy_slot="not_found") :
        self.scsi_host_number = scsi_host_number
        self.name = name
        self.sas_wwid = sas_wwid
        self.firmware_version = firmware_version
        self.sysfs_dir = sysfs_dir
        self.sysfs_device_dir = sysfs_device_dir
        self.pci_device_info = pci_device_info
        self.phy_slot = phy_slot

def find(pattern, string, *group):
    match = re.search(pattern, string, re.MULTILINE)
    return match.group(*group) if match else ""

def find_all(pattern, string, *group):
    return [ match.group(*group) for match in re.finditer(pattern, string, re.MULTILINE) ]

def display_summary(adapters):
    colum_items = ['scsi_host_number', 'phy_slot', 'name', 'sas_wwid', 'firmware_version', 'sysfs_dir', 'sysfs_device_dir']
    colum_names = ['[H]', 'Slot', 'Name', 'SAS WWID', 'FW. Rev', 'Sysfs dir', 'Sysfs device dir']
    cell_width  = [ len(name)+1 for name in colum_names ]
    contents_length = vars(HostBusAdapter())
    for item in contents_length:
        contents_length[item] = len(max([ getattr(adapter, item) for adapter in adapters ], key=len))
    output_top       = "┌─"
    output_colum     = "│ "
    output_separator = "├─"
    output_bottom    = "└─"
    for i in range (len(colum_items)) :
        colum_name_length = len(colum_names[i])
        cell_width[i] = max(contents_length[colum_items[i]]+1, cell_width[i])
        if i != 0 :
            output_top       = output_top       + "┬─"
            output_colum     = output_colum     + "│ "
            output_separator = output_separator + "┴─"
            output_bottom    = output_bottom    + "──"
        output_top       = output_top       + "─"*len(colum_names[i]) + "─"*(cell_width[i] - colum_name_length)
        output_colum     = output_colum     +         colum_names[i]  + " "*(cell_width[i] - colum_name_length)
        output_separator = output_separator + "─"*len(colum_names[i]) + "─"*(cell_width[i] - colum_name_length)
        output_bottom    = output_bottom    + "─"*len(colum_names[i]) + "─"*(cell_width[i] - colum_name_length)
    output_top       = output_top       + "┐"
    output_colum     = output_colum     + "│"
    output_separator = output_separator + "┤"
    output_bottom    = output_bottom    + "┘"
    print output_top
    print output_colum
    print output_separator
    for adapter in adapters :
        output_row = "│ "
        for i in range (len(colum_items)) :
            if i != 0 :
                output_row = output_row + "  "
            output_row = output_row + getattr(adapter, colum_items[i]) + " "*(cell_width[i] - len(getattr(adapter, colum_items[i])))
        output_row = output_row + "│"
        print output_row
    print output_bottom

def display_details(adapters):
    print("═"*80)
    print("")
    for a, adapter in enumerate(adapters) :
        print "SCSI adapter(host) number[H]: {0}".format(adapter.scsi_host_number)
        print "  Slot: " + adapter.phy_slot
        print "  Name: " + adapter.name
        print "  SAS WWID: " + adapter.sas_wwid
        print "  FW. Rev: " + adapter.firmware_version
        print "  Sysfs dir: " + adapter.sysfs_dir
        print "  Sysfs device dir: " + adapter.sysfs_device_dir
        print "  Device info: " + adapter.pci_device_info
        if a < len(adapters) - 1 :
            print("━"*80)
        print("")
    print("═"*80)

def get_scsi_host_bus_adapters(scsi_host_numbers):
    adapters = []
    for scsi_host_number in scsi_host_numbers:
        scsi_host_info = os.popen("lsscsi --hosts --transport --verbose "+scsi_host_number).read()
        scsi_host_sysfs_dir = find(r"^  dir: (.*)", scsi_host_info, 1)
        scsi_host_sysfs_device_dir = find(r"^  device dir: (.*)", scsi_host_info, 1)
        scsi_host_pci_device_slot_id = basename(dirname(scsi_host_sysfs_device_dir))
        scsi_host_pci_device_info = os.popen("lspci -v -s "+scsi_host_pci_device_slot_id).read().replace("\t", "    ")
        pci_bus_address = find(r"/pci[\w:\.]+/[\w:\.]+/([\w:\.]+)/", scsi_host_sysfs_device_dir, 1)
        dmi_slot_info = os.popen("dmidecode --type slot").read()
        scsi_host_phy_slot = find(r"System Slot Information\n\s+Designation: (.*)\n(?:.*\n){,10}\s+Bus Address: "+pci_bus_address.replace(".",r"\."), dmi_slot_info, 1)
        adapters.append(HostBusAdapter(
            scsi_host_number,
            find(r"Subsystem: (.*)", scsi_host_pci_device_info, 1),
            find(r"sas:0x(.*)", scsi_host_info, 1),
            open(scsi_host_sysfs_dir+"/version_fw", "r").read().rstrip() if isfile(scsi_host_sysfs_dir+"/version_fw") else "not_found",
            scsi_host_sysfs_dir,
            scsi_host_sysfs_device_dir,
            scsi_host_pci_device_info,
            scsi_host_phy_slot if scsi_host_phy_slot != "" else "not_found",
        ))
    return adapters


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='List SCSI Host Bus Adapter information.')
    parser.add_argument('scsi_host_numbers', nargs='*',
                        help='SCSI adapter(host) number[H] to show details')
    args = parser.parse_args()

    if len(args.scsi_host_numbers) > 0 :
        scsi_host_numbers = args.scsi_host_numbers
        adapters = get_scsi_host_bus_adapters(scsi_host_numbers)
        display_details(adapters)
    else :
        scsi_host_numbers = find_all(r"^\[(\d+)\]", os.popen("lsscsi --hosts | grep 'sas'").read(), 1)
        adapters = get_scsi_host_bus_adapters(scsi_host_numbers)
        display_summary(adapters)
