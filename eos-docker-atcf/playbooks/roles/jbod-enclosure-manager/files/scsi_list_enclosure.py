#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import re
import argparse
import textwrap
from abc import *
from os.path import basename, dirname, isfile
from operator import attrgetter

try:
    from itertools import zip_longest
except ImportError:
    from itertools import izip_longest as zip_longest

try:
    import textwrap
    textwrap.indent
except AttributeError:
    def indent(text, prefix, predicate=None):
        if predicate is None:
            def predicate(line):
                return line.strip()
        def prefixed_lines():
            for line in text.splitlines(True):
                yield (prefix + line if predicate(line) else line)
        return ''.join(prefixed_lines())
else:
    def indent(text, prefix, predicate=None):
        return textwrap.indent(text, prefix, predicate=predicate)


def find(pattern, string, *group):
    match = re.search(pattern, string, re.MULTILINE)
    return match.group(*group) if match else ""

def find_all(pattern, string, *group):
    return [ match.group(*group) for match in re.finditer(pattern, string, re.MULTILINE) ]

def display_summary(enclosures):
    colum_items = ['scsi_addresses', 'scsi_devices', 'scsi_pci_slots', 'wwid', 'name', 'number_of_disk_and_slot', 'status']
    colum_names = ['SCSI address[H:C:T:L]', 'Device', 'PCI Slot', 'WWID', 'Name', '#.Disk', 'Status']
    cell_width  = [ len(name)+1 for name in colum_names ]
    contents = dict(zip_longest(colum_items, []))
    contents_length = dict(zip_longest(colum_items, []))
    for colum_item in colum_items :
        contents_of_colum = []
        for contents_item in [ getattr(enclosure, colum_item) for enclosure in enclosures ] :
            if callable(contents_item) :
                contents_item = contents_item()
            if isinstance(contents_item, str) :
                contents_of_colum.append(contents_item.split('\n'))
            elif isinstance(contents_item, list) :
                contents_of_colum.append(contents_item)
            else :
                contents_of_colum.append(["Cannot Display: "+str(type(contents_item))])
        contents[colum_item] = contents_of_colum
        # flatten 2D array and find max length of string
        contents_length[colum_item] = len(max([v for sublist in contents[colum_item] for v in sublist], key=len))
    output_top       = "┌─"
    output_colum     = "│ "
    output_separator = "├─"
    output_bottom    = "└─"
    for i in range (len(colum_items)) :
        colum_name_length = len(colum_names[i])
        cell_width[i] = max(contents_length[colum_items[i]]+1, cell_width[i])
        if i != 0 :
            output_top       = output_top       + "┬─"
            output_colum     = output_colum     + "│ "
            output_separator = output_separator + "┴─"
            output_bottom    = output_bottom    + "──"
        output_top       = output_top       + "─"*len(colum_names[i]) + "─"*(cell_width[i] - colum_name_length)
        output_colum     = output_colum     +         colum_names[i]  + " "*(cell_width[i] - colum_name_length)
        output_separator = output_separator + "─"*len(colum_names[i]) + "─"*(cell_width[i] - colum_name_length)
        output_bottom    = output_bottom    + "─"*len(colum_names[i]) + "─"*(cell_width[i] - colum_name_length)
    output_top       = output_top       + "┐"
    output_colum     = output_colum     + "│"
    output_separator = output_separator + "┤"
    output_bottom    = output_bottom    + "┘"
    print(output_top)
    print(output_colum)
    print(output_separator)
    for k in range (len(enclosures)) :
        contents_of_enclosure = [ contents[colum_item][k] for colum_item in colum_items ]
        contents_height = len(max(contents_of_enclosure, key=len))
        for j in range (contents_height) :
            output_row = "│ "
            for i in range (len(colum_items)) :
                if i != 0 :
                    output_row = output_row + "  "
                if j < len(contents_of_enclosure[i]) :
                    output_row = output_row + contents_of_enclosure[i][j] + " "*(cell_width[i] - len(contents_of_enclosure[i][j]))
                else :
                    output_row = output_row + " "*cell_width[i]
            output_row = output_row + "│"
            print(output_row)
        if contents_height > 1 and k < len(enclosures)-1 :
            print("│ " + "─"*(len(output_row) - len(cell_width) - 1) + " │")
    print(output_bottom)

def display_details_body(body_lines, max_lines, prefix, tree_shapes):
    count = min(len(body_lines), max_lines)
    if count > 1 :
        print(indent(body_lines[0], prefix+tree_shapes[0]))
        if count > 2 :
            print(indent("\n".join(body_lines[1:count-1]), prefix+tree_shapes[1], lambda line: True))
        print(indent(body_lines[count-1], prefix+tree_shapes[2]))
    elif count == 1 :
        print(indent(body_lines[0],  prefix+tree_shapes[3]))
    else :
        print("")

def display_details_body_next(body_lines, max_lines, prefix):
    display_details_body(body_lines, max_lines, prefix, ["├┬▶", "││ ", "│└◉", "├─▶"])

def display_details_body_last(body_lines, max_lines, prefix):
    display_details_body(body_lines, max_lines, prefix, ["└┬▶", " │ ", " └◉", "└─▶"])

def display_details(enclosures):
    print("═"*80)
    print("")
    for e, enclosure in enumerate(enclosures) :
        print("▪ Summary:")
        display_summary([enclosure])
        print("")
        print("▪ Sysfs dir:")
        display_details_body_last(enclosure.scsi_sysfs_dirs, 10, "  ")
        print("")
        print("╴"*80)
        print("")
        print("▪ Enclosure Infomation: ")
        enclosure.ses_enclosure_info_bodies
        for i, body in enumerate(enclosure.ses_enclosure_info_bodies) :
            if i < len(enclosure.ses_enclosure_info_bodies) - 1 :
                display_details_body_next(body.splitlines(), 6, "  ")
                print("  │")
            else :
                display_details_body_last(body.splitlines(), 6, "  ")
        print("╴"*80)
        print("")
        print("▪ Controller Infomation: ")
        for i in range(len(enclosure.controller_wwids)) :
            body = []
            body.append("Slot "+enclosure.ses_controller_lables[i]+": " + ','.join(enclosure.controller_wwids[i]) + "     Status: " + ','.join(enclosure.controller_status[i]))
            for j, reorder in enumerate(enclosure.ses_controller_order_to_scsi_connection_order[i]) :
                if reorder != None :
                    body.append(
                        "  Connector " +enclosure.ses_controller_sas_connector_lables[i][j]+" : "+enclosure.controller_sas_connector_wwns[i][j]+
                        " ⇒  "+enclosure.scsi_connected_adapter_phy_slots[reorder]+" Adapter["+enclosure.scsi_connected_adapter_numbers[reorder]+"]: "+enclosure.scsi_connected_adapter_names[reorder]+
                        " : Port "+enclosure.scsi_connected_adapter_port_numbers[reorder]+" : "+enclosure.scsi_connected_adapter_wwns[reorder])
            if i < len(enclosure.controller_wwids) - 1 :
                display_details_body_next(body, 5, "  ")
                print("  │")
            else :
                display_details_body_last(body, 5, "  ")
        print("╴"*80)
        print("")
        print("▪ Overall Status: ")
        body = []
        for overall_status in enclosure.overall_status :
            body.append(overall_status["name"]+": "+overall_status["status"])
        display_details_body_last(body, 20, "  ")
        if e < len(enclosures) - 1 :
            print("━"*80)
        print("")
    print("═"*80)

def display_full_details(enclosures):
    print("═"*80)
    print("")
    for e, enclosure in enumerate(enclosures) :
        for i, element_id in enumerate([ int(item) for item in enclosure.ses_overall_indices ]) :
            print("▪ "+enclosure.overall_status[i]["name"]+": ")
            summary = enclosure.ses_join_info_summary_by_element[element_id]
            sub_items = enclosure.ses_join_info_sub_items_by_element[element_id]
            if len(summary) > 0 or len(sub_items) > 0 :
                print("  │")
                if len(summary) > 0 and len(sub_items) > 0 :
                    display_details_body_next(summary.splitlines(), 9999, "  ")
                    print("  │")
                elif len(summary) > 0 :
                    display_details_body_last(summary.splitlines(), 9999, "  ")
                else :
                    pass
                for j, sub_item in enumerate(sub_items) :
                    if j < len(sub_items) - 1 :
                        display_details_body_next(sub_item.splitlines(), 9999, "  ")
                        print("  │")
                    else :
                        display_details_body_last(sub_item.splitlines(), 9999, "  ")
            if i < len(enclosure.ses_overall_indices) - 1 :
                print("╴"*80)
                print("")
        if e < len(enclosures) - 1 :
            print("━"*80)
        print("")
    print("═"*80)


class lazy_property(object):
    def __init__(self, getter):
        self.getter = getter
    def __get__(self, instance, cls):
        # print "lazy_property: " + self.getter.__name__
        if not instance.initialized:
            raise ValueError("initialization is not completed")
        value = self.getter(instance)
        setattr(instance, self.getter.__name__, value)
        return value

class Enclosure:
    def __init__(self,
        scsi_addresses=["unknown"], scsi_devices=["unknown"], scsi_port_wwns=["unknown"], scsi_sysfs_dirs=["unknown"], scsi_pci_slots=["unknown"],
        ses_cf_info="unknown", wwid="not_found"):
        if not isinstance(scsi_addresses,list):
            raise ValueError("scsi_addresses is not list")
        if not isinstance(scsi_devices,list):
            raise ValueError("scsi_devices is not list")
        if not isinstance(scsi_port_wwns,list):
            raise ValueError("scsi_port_wwns is not list")
        if not isinstance(scsi_sysfs_dirs,list):
            raise ValueError("scsi_sysfs_dirs is not list")
        if not isinstance(scsi_pci_slots,list):
            raise ValueError("scsi_pci_slots is not list")
        self.initialized = False
        self.__scsi_addresses = scsi_addresses
        self.__scsi_devices = scsi_devices
        self.__scsi_port_wwns = scsi_port_wwns
        self.__scsi_sysfs_dirs = scsi_sysfs_dirs
        self.__scsi_pci_slots = scsi_pci_slots
        self.__ses_cf_info = ses_cf_info
        self.__ses_join_info = ""
        self.__wwid = wwid
        self.__name = "not_found"
        self.__number_of_disk_slot = "not_found"
        self.__number_of_disk = "not_found"
        self.__number_of_disk_and_slot = str(self.__number_of_disk) + "/" + str(self.__number_of_disk_slot)
        for name in self.ses_element_names :
            def info_getter(self, n=name):
                return self.get_ses_join_info_of_element_indices(getattr(self, "ses_"+n+"_indices"))
            def title_getter(self, n=name):
                return self.get_titles_of_ses_join_info_of_elements(getattr(self, "ses_"+n+"_indices"), getattr(self, "ses_"+n+"_info"))
            def body_getter(self, n=name):
                return self.get_bodies_of_ses_join_info_of_elements(getattr(self, "ses_"+n+"_indices"), getattr(self, "ses_"+n+"_info"))
            info_getter.__name__ = "ses_"+name+"_info"
            title_getter.__name__ = "ses_"+name+"_info_titles"
            body_getter.__name__ = "ses_"+name+"_info_bodies"
            setattr(self.__class__, "ses_"+name+"_info", lazy_property(info_getter))
            setattr(self.__class__, "ses_"+name+"_info_titles", lazy_property(title_getter))
            setattr(self.__class__, "ses_"+name+"_info_bodies", lazy_property(body_getter))
        for name in self.ses_sub_element_names :
            def info_getter(self, n=name):
                info = []
                for sub_indices in getattr(self, "ses_"+n+"_indices") :
                    info.append(self.get_ses_join_info_of_element_indices(sub_indices))
                return info
            def title_getter(self, n=name):
                info = []
                for i, sub_indices in enumerate(getattr(self, "ses_"+n+"_indices")) :
                    info.append(self.get_titles_of_ses_join_info_of_elements(sub_indices), getattr(self, "ses_"+n+"_info")[i])
                return info
            def body_getter(self, n=name):
                info = []
                for i, sub_indices in enumerate(getattr(self, "ses_"+n+"_indices")) :
                    info.append(self.get_bodies_of_ses_join_info_of_elements(sub_indices), getattr(self, "ses_"+n+"_info")[i])
                return info
            info_getter.__name__ = "ses_"+name+"_info"
            title_getter.__name__ = "ses_"+name+"_info_titles"
            body_getter.__name__ = "ses_"+name+"_info_bodies"
            setattr(self.__class__, "ses_"+name+"_info", lazy_property(info_getter))
            setattr(self.__class__, "ses_"+name+"_info_titles", lazy_property(title_getter))
            setattr(self.__class__, "ses_"+name+"_info_bodies", lazy_property(body_getter))
    @property
    def scsi_addresses(self):
        return self.__scsi_addresses
    @property
    def scsi_devices(self):
        return self.__scsi_devices
    @property
    def scsi_port_wwns(self):
        return self.__scsi_port_wwns
    @property
    def scsi_sysfs_dirs(self):
        return self.__scsi_sysfs_dirs
    @property
    def scsi_pci_slots(self):
        return self.__scsi_pci_slots
    @property
    def ses_cf_info(self):
        return self.__ses_cf_info
    @lazy_property
    def ses_join_info(self):
        return os.popen("sg_ses "+self.scsi_devices[0]+" -jj").read()
    @lazy_property
    def scsi_sub_item_info(self):
        info = ""
        for host_address in set([ address.split(':',1)[0] for address in self.scsi_addresses ]) :
            info = info + os.popen("lsscsi -gitv "+host_address).read()
        return info
    @property
    def wwid(self):
        return self.__wwid
    @lazy_property
    def name(self):
        # get first line and substitute multiple whitespace with single whitespace
        return ' '.join(self.ses_cf_info.split('\n', 1)[0].split())
    @lazy_property
    def number_of_disk_slot(self):
        return int(find(r"Array device slot,.*\n.*number of possible elements: (\d+)", self.ses_cf_info, 1))
    @lazy_property
    def number_of_disk(self):
        disk_count = 0
        for host_address in set([ address.split(':',1)[0] for address in self.scsi_addresses ]) :
            disk_count += len(find_all(r"disk    0x", os.popen("lsscsi -w "+host_address).read()))
        return disk_count
    @lazy_property
    def number_of_disk_and_slot(self):
        return str(self.number_of_disk) + "/" + str(self.number_of_disk_slot)
    @lazy_property
    def status(self):
        status = []
        for i, info in enumerate(self.ses_enclosure_info) :
            status_item = find(r", status: (\w+)$", info, 1)
            status.append(status_item)
        return status
    @lazy_property
    def controller_wwids(self):
        wwid = []
        for i, sub_item in enumerate(self.ses_controller_sas_expander_info) :
            sub_wwid = []
            for j, info in enumerate(sub_item) :
                sub_wwid_item = int(find(r"SAS address: 0x(\w+)$", info, 1), 16) + self.ses_controller_sas_expander_address_offsets_to_wwid[i][j]
                sub_wwid.append(hex(sub_wwid_item)[2:])
            wwid.append(sub_wwid)
        return wwid
    @lazy_property
    def controller_status(self):
        status = []
        for sub_item in self.ses_controller_sas_expander_info :
            sub_status = []
            for info in sub_item :
                sub_status.append(find(r", status: (\w+)$", info, 1))
            status.append(sub_status)
        return status
    @lazy_property
    def controller_sas_connector_wwns(self):
        wwns = []
        for ses_controller_sas_connector_info in self.ses_controller_sas_connector_info :
            wwns.append([ find(r"WN=(\w+)", item, 1).lower() for item in ses_controller_sas_connector_info ])
        return wwns
    @property
    def ses_element_names(self):
        raise NotImplementedError
    @property
    def ses_sub_element_names(self):
        raise NotImplementedError
    @lazy_property
    def scsi_connected_adapter_wwns(self):
        return [ find(r"sas:0x(.*)", os.popen("lsscsi --hosts --transport --verbose "+item).read(), 1) for item in self.scsi_connected_adapter_numbers ]
    @lazy_property
    def scsi_connected_adapter_names(self):
        return [ find(r"Subsystem: (.*)", os.popen("lspci -v -s "+basename(item)).read().replace("\t", "    "), 1) for item in self.scsi_pci_slots ]
    @lazy_property
    def scsi_connected_adapter_numbers(self):
        return [ find(r"/host(\d+)/port-\d+:\d+/expander-\d+:\d+/port-\d+:\d+:\d+/", item, 1) for item in self.scsi_sysfs_dirs ]
    @lazy_property
    def scsi_connected_adapter_port_numbers(self):
        return [ find(r"/host\d+/port-\d+:(\d+)/expander-\d+:\d+/port-\d+:\d+:\d+/", item, 1) for item in self.scsi_sysfs_dirs ]
    @lazy_property
    def scsi_connected_adapter_phy_slots(self):
        pci_bus_addresses = [ find(r"/pci[\w:\.]+/[\w:\.]+/([\w:\.]+)/", item, 1) for item in self.scsi_sysfs_dirs ]
        dmi_slot_info = os.popen("dmidecode --type slot").read()
        return [ find(r"System Slot Information\n\s+Designation: (.*)\n(?:.*\n){,10}\s+Bus Address: "+item.replace(".",r"\."), dmi_slot_info, 1) for item in pci_bus_addresses ]
    @lazy_property
    def scsi_connected_enclosure_port_numbers(self):
        return [ find(r"/host\d+/port-\d+:\d+/expander-\d+:\d+/port-\d+:(\d+):\d+/", item, 1) for item in self.scsi_sysfs_dirs ]
    @lazy_property
    def ses_controller_order_to_scsi_connection_order(self):
        reorder = []
        for i, controller_wwids in enumerate(self.controller_wwids) :
            reorder_item = []
            for j, connector_wwn in enumerate(self.controller_sas_connector_wwns[i]) :
                controller_wwid = controller_wwids[0]
                for k in range(len(self.scsi_port_wwns)) :
                    if controller_wwid[:-1] == self.scsi_port_wwns[k][:-1] and connector_wwn[:-1] == self.scsi_connected_adapter_wwns[k][:-1] :
                        reorder_item.append(k)
                        break
                    if k == len(self.scsi_port_wwns) -1 :
                        reorder_item.append(None)
            reorder.append(reorder_item)
        return reorder
    @lazy_property
    def overall_status(self):
        overall_status = []
        for i in [ int(item) for item in self.ses_overall_indices ] :
            ses_join_info = self.ses_join_info_by_element[i]
            name = find(r"^.*\[\d+,-1\]\s+Element type:\s+(.*)", ses_join_info, 1)
            if name.startswith("vendor specific") :
                element_id = find(r"vendor specific \[(.*)\]", name, 1)
                name = find(r"Element type: vendor specific \["+element_id+r"\], [\s\S]+? text: (.*)\n(?:[\s\S]+?Element type:|\Z)", self.ses_cf_info, 1)
            status = find(r"^.*\[\d+,-1\](?:.*\n){2}.*, status: (.*)", ses_join_info, 1)
            if status == "Unsupported" :
                status = "|".join(set(find_all(r"^.*\[\d+,\d+\](?:.*\n){2}.*, status: (?!Not installed)(.*)", ses_join_info, 1)))
            overall_status.append({ "name": name, "status": status })
        return overall_status
    @lazy_property
    def ses_join_info_by_element(self):
        # ^.*\[(\d+),-1\][\s\S]+?(?=^.*\[\d+,-1\]|\Z)
        return find_all(r"^.*\[(\d+),-1\][\s\S]+?(?=^.*\[\d+,-1\]|\Z)", self.ses_join_info)
    @lazy_property
    def ses_join_info_summary_by_element(self):
        return [ find(r"^.*\[\d+,-1\][\s\S]+?(?=^.*\[\d+,0\]|\Z)", item) for item in self.ses_join_info_by_element ]
    @lazy_property
    def ses_join_info_sub_items_by_element(self):
        sub_items_by_element = [ find_all(r"^.*\[\d+,\d+\][\s\S]+?(?=^.*\[\d+,\d+\]|\Z)", item) for item in self.ses_join_info_by_element ]
        for i, sub_items in enumerate(sub_items_by_element) :
            for j, sub_item in enumerate(sub_items) :
                sub_title = find(r"^.*(\[\d+,\d+\].*\n)", sub_item, 1)
                sub_body_group = find(r"(^.*)(?:\[\d+,\d+\].*\n)([\s\S]+?)(?=^.*\[\d+,\d+\]|\Z)", sub_item, 1, 2)
                if sub_body_group[0] != "" :
                    sub_body = "  "+sub_body_group[0]+"\n"+sub_body_group[1]
                else :
                    sub_body = sub_body_group[1]
                sub_items[j] = sub_title + sub_body
            sub_items_by_element[i] = sub_items
        return sub_items_by_element
    def get_ses_join_info_of_element_indices(self, element_indices):
        info = []
        for index in element_indices :
            info_item = find(r"^.*\["+index+r"\][\s\S]+?(?=^.*\[\d+,-?\d+\]|\Z)", self.ses_join_info)
            # handling extra information
            start_position = info_item.find("["+index+"]")
            if start_position != 0 :
                info_item = info_item[:start_position] + "\n" + info_item[start_position:]
            info.append(info_item)
        return info
    def get_titles_of_ses_join_info_of_elements(self, element_indices, ses_join_info_of_elements):
        titles = []
        for i in range(len(element_indices)) :
            title = find(r"^\["+element_indices[i]+r"\].*$", ses_join_info_of_elements[i])
            titles.append(title)
        return titles
    def get_bodies_of_ses_join_info_of_elements(self, element_indices, ses_join_info_of_elements):
        bodies = []
        for i in range(len(element_indices)) :
            # ([\s\S]*?)(?:^\[7,0\].*$\n)([\s\S]*)
            body = ''.join(find(r"([\s\S]*?)(?:^\["+element_indices[i]+r"\].*$\n)([\s\S]*)", ses_join_info_of_elements[i], 1, 2))
            bodies.append(body)
        return bodies

class Enclosure_DellEMC_Array584EMM(Enclosure):
    @property
    def ses_element_names(self):
        return [ "enclosure", "controller", "controller_sas_expander" ]
    @property
    def ses_sub_element_names(self):
        return [ "controller_sas_expander", "controller_sas_connector" ]
    @property
    def ses_enclosure_indices(self):
        return [ "7,0" ]
    @property
    def ses_controller_indices(self):
        return [ "6,0", "6,1" ]
    @property
    def ses_controller_lables(self):
        return [ "A", "B" ]
    @property
    def ses_controller_sas_expander_indices(self):
        return [ ["10,8"], ["10,9"] ]
    @property
    def ses_controller_sas_expander_address_offsets_to_wwid(self):
        return [ [-1], [-1] ]
    @property
    def ses_controller_sas_connector_indices(self):
        return [ ["11,0", "11,1", "11,2"], ["11,3", "11,4", "11,5"] ]
    @property
    def ses_controller_sas_connector_lables(self):
        return [ ["A", "B", "C"], ["A", "B", "C"] ]
    @property
    def ses_overall_indices(self):
        return [ "7", "6", "0", "1", "2", "3", "4", "5", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17" ]

class EnclosureDirector:
    @staticmethod
    def construct(scsi_addresses, scsi_devices, scsi_port_wwns, scsi_sysfs_dirs, scsi_pci_slots, ses_cf_info, wwid):
        subfix = '_'.join(find(r"enclosure vendor:\W+(\w+)\W+product:\W+(\w+)\W+", ses_cf_info, 1, 2))
        targetClass = globals()['Enclosure_'+subfix]
        instance = targetClass(scsi_addresses, scsi_devices, scsi_port_wwns, scsi_sysfs_dirs, scsi_pci_slots, ses_cf_info, wwid)
        return instance

def get_scsi_enclosures(scsi_enclosure_addresses):
    enclosures = {}
    for scsi_enclosure_address in scsi_enclosure_addresses :
        scsi_enclosure_info = os.popen("lsscsi --transport --generic --verbose "+scsi_enclosure_address).read()
        scsi_enclosure_device = find(r"(/dev/sg\d+)", scsi_enclosure_info, 1)
        scsi_enclosure_port_wwn = find(r"sas:0x(\w+)", scsi_enclosure_info, 1)
        scsi_enclosure_sysfs_dir = find(r"^  dir: (.*)", scsi_enclosure_info, 1)
        scsi_enclosure_pci_slot = find(r"/(pci[\w:\.]+/[\w:\.]+/[\w:\.]+)/", scsi_enclosure_sysfs_dir, 1)
        ses_cf_info = os.popen("sg_ses "+scsi_enclosure_device+" -p cf").read()
        enc_wwid = find(r"enclosure logical identifier \(hex\): (.*)", ses_cf_info, 1)
        if enc_wwid not in enclosures :
            enclosures[enc_wwid] = EnclosureDirector.construct(
                [scsi_enclosure_address],
                [scsi_enclosure_device],
                [scsi_enclosure_port_wwn],
                [scsi_enclosure_sysfs_dir],
                [scsi_enclosure_pci_slot],
                ses_cf_info,
                enc_wwid
            )
        else :
            enclosure = enclosures[enc_wwid]
            enclosure.scsi_addresses.append(scsi_enclosure_address)
            enclosure.scsi_devices.append(scsi_enclosure_device)
            enclosure.scsi_port_wwns.append(scsi_enclosure_port_wwn)
            enclosure.scsi_sysfs_dirs.append(scsi_enclosure_sysfs_dir)
            enclosure.scsi_pci_slots.append(scsi_enclosure_pci_slot)
    for enclosure in enclosures.values() :
        enclosure.initialized = True
    return list(enclosures.values())


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='List SCSI Enclosure information.')
    parser.add_argument('-f', '--full', action='store_true',
                        help='')
    parser.add_argument('scsi_enclosure_addresses', nargs='*',
                        help='SCSI enclosure address[H:C:T:L] to show details')
    args = parser.parse_args()

    if len(args.scsi_enclosure_addresses) > 0 :
        scsi_enclosure_addresses = args.scsi_enclosure_addresses
        enclosures = get_scsi_enclosures(scsi_enclosure_addresses)
        if args.full :
            display_full_details(enclosures)
        else :
            display_details(enclosures)
    else :
        scsi_enclosure_addresses = find_all(r"^\[([\d:]+)\]", os.popen("lsscsi | grep 'enclosu'").read(), 1)
        enclosures = get_scsi_enclosures(scsi_enclosure_addresses)
        if args.full :
            display_full_details(enclosures)
        else :
            display_summary(enclosures)
